import axios from 'axios'
import { getToken } from '@/utils/auth'

const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: process.env.VUE_APP_BASE_API,
  // 超时
  timeout: 10000
})
service.interceptors.request.use(config => { // 是否需要设置 token
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const isToken = (config.headers || {}).isToken === '0'
  if (getToken() && !isToken) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // config.headers.Authorization = 'Bearer ' + getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
    config.headers.Authorization = 'Bearer ' + getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  // get请求映射params参数
  // if (config.method === 'get' && config.params) {
  //   let url = config.url + '?' + tansParams(config.params)
  //   url = url.slice(0, -1)
  //   config.params = {}
  //   config.url = url
  // }
  return config
}, error => {
  console.log(error)
  Promise.reject(error)
})
service.interceptors.response.use((res) => {
  console.log(res.data)
  return res.data
})

export default service
