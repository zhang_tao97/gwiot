import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import Layout from '../layout/index.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/404',
    component: () => import('../views/error/404.vue')
  },
  {
    path: '/401',
    component: () => import('../views/error/401.vue')
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/index.vue')
      },
      {
        path: 'deviceMetrics',
        component: () => import('@/views/deviceMetrics/index.vue')
      },
      {
        path: 'deviceManage',
        component: () => import('@/views/deviceManage/index.vue')
      },
      {
        path: 'metricManage',
        component: () => import('@/views/metricManage/index.vue')
      },
      {
        path: 'alarmManage',
        component: () => import('@/views/alarmManage/index.vue')
      },
      {
        path: 'alarmLog',
        component: () => import('@/views/alarmLog/index.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
