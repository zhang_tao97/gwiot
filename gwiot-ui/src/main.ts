import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import 'element-plus/theme-chalk/display.css'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/permission'
import Moment from 'moment'
const app = createApp(App)
app.use(store).use(router).use(ElementPlus, {
  locale: zhCn
}).mount('#app')
app.config.globalProperties.moment = Moment
