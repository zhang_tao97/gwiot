import router from './router'
import { UserModule } from '@/store/modules/user'
const whiteList = ['/login', '404']
router.beforeEach((to, from, next) => {
  if (UserModule.token) {
    if (to.path === '/login') {
      next('/')
    } else {
      next()
    }
  } else {
    // 在数组中没找到指定元素则返回 -1
    if (whiteList.indexOf(to.path) > -1) {
      next()
    } else {
      next('/login')
    }
  }
})
