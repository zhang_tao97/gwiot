import request from '@/utils/request'

// 登录方法
export function login (data:unknown) {
  return request({
    url: '/login',
    headers: {
      isToken: '0'
    },
    method: 'post',
    data: data
  })
}

// 退出方法
export function logout () {
  return request({
    url: '/logout',
    method: 'post'
  })
}
