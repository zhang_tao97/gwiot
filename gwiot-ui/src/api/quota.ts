import request from '@/utils/request'

export const quota = (params: any) =>
  request({
    url: '/quota',
    method: 'get',
    params
  })
export const addQuota = (data: any) =>
  request({
    url: '/quota',
    method: 'post',
    data
  })
export const putQuota = (data: any) =>
  request({
    url: '/quota',
    method: 'put',
    data
  })
export const delQuota = (id: any) =>
  request({
    url: `/quota/${id}`,
    method: 'delete'
  })
