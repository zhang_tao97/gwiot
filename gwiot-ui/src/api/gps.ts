import request from '@/utils/request'

export const gps = () =>
  request({
    url: '/gps',
    method: 'get'
  })
export const addGps = (data: any) =>
  request({
    url: '/gps',
    method: 'post',
    data
  })
export const putGps = (data: any) =>
  request({
    url: '/gps',
    method: 'put',
    data
  })
