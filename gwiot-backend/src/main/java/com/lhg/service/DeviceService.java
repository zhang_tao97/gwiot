package com.lhg.service;

import com.lhg.pojo.dto.DeviceDTO;
import com.lhg.pojo.vo.DeviceQuotaVO;
import com.lhg.pojo.vo.Pager;

public interface DeviceService  {
    /**
     * 搜索设备
     * @param page
     * @param pageSize
     * @param sn
     * @param tag
     * @return
     */
    Pager<DeviceDTO> queryPage(Long page, Long pageSize, String sn, String tag, Integer status);

    Pager<DeviceQuotaVO> queryDeviceQuota(Long page, Long pageSize, String deviceId, String tag, Integer state);

    /**
     * 存储设备信息
     * @param deviceDTO
     * @return
     */
    boolean saveDeviceInfo(DeviceDTO deviceDTO);

    /**
     * 更新在线状态
     * @param deviceId
     * @param online
     */
    void updateOnLine(String deviceId, Boolean online);

    /**
     * 断网警告
     * @param deviceId
     */
    void disconnectionAlarm(String deviceId);

    boolean setStatus(String sn, Boolean status);

    boolean updateTags(String sn, String tags);
}
