package com.lhg.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lhg.pojo.dto.DeviceInfoDTO;
import com.lhg.pojo.dto.QuotaAllInfo;
import com.lhg.pojo.dto.QuotaDTO;
import com.lhg.pojo.entity.Alarm;
import com.lhg.pojo.vo.Pager;

import java.util.List;

public interface AlarmService extends IService<Alarm> {


    /**
     *
     * @param page
     * @param pageSize
     * @param start
     * @param end
     * @param alarmName
     * @param deviceId
     * @return
     */
    Pager<QuotaAllInfo> queryAlarmLog(Long page, Long pageSize, String start, String end, String alarmName, String deviceId);


    /**
     * 获取某一指标下的所有告警设置
     * @param quotaId 指标Id
     * @return
     */
    List<Alarm> getByQuotaId(Integer quotaId);
    /**
     * 根据指标判断告警信息
     * @param quotaDTO 指标dto
     * @return Alarm 告警对象
     */
    Alarm verifyQuota(QuotaDTO quotaDTO);

    /**
     * 根据设备信息判断
     * @param deviceInfoDTO 设备信息dto
     * @return DeviceInfoDTO 验证后设备信息dto
     */
    DeviceInfoDTO verifyDeviceInfo(DeviceInfoDTO deviceInfoDTO);

    /**
     * 分页查询告警设置
     * @param page
     * @param pageSize
     * @param alarmName
     * @param quotaId
     * @return
     */
    IPage<Alarm> queryPage(Long page, Long pageSize, String alarmName, Integer quotaId);

}
