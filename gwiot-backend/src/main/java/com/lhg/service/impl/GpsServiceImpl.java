package com.lhg.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.lhg.framework.emq.EmqClient;
import com.lhg.framework.es.ESRepository;
import com.lhg.mapper.GpsMapper;
import com.lhg.pojo.dto.DeviceDTO;
import com.lhg.pojo.dto.DeviceFullInfo;
import com.lhg.pojo.dto.DeviceLocation;
import com.lhg.pojo.dto.QuotaInfo;
import com.lhg.pojo.entity.GPS;
import com.lhg.service.GpsService;
import com.lhg.service.QuotaService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class GpsServiceImpl extends ServiceImpl<GpsMapper, GPS> implements GpsService {

    @Autowired
    private ESRepository esRepository;

    @Autowired
    private QuotaService quotaService;

    @Autowired
    private EmqClient emqClient;
    @Override
    public DeviceLocation analysis(String topic, Map<String, Object> payloadMap) {
        //读取规则
        GPS gpsEntity = getGps();
        if(gpsEntity == null) {
            return null;
        }
        if(Strings.isNullOrEmpty(gpsEntity.getSubject())) {
            return null;// 主题为空
        }
        if(!topic.equals(gpsEntity.getSubject())) {
            return null;//如果主题不匹配
        }

        //读取设备id
        String deviceId = "";
        deviceId = (String) payloadMap.get(gpsEntity.getSnKey());
        if(Strings.isNullOrEmpty(deviceId)) {
            return null;
        }

        //提取gps
        String location = "";
        if(gpsEntity.getSingleField()){ //如果是单字段
            location = ((String) payloadMap.get(gpsEntity.getValueKey())).replace(gpsEntity.getSeparation(),",");
        }else { //如果是双字段
            location = payloadMap.get(gpsEntity.getLongitude()) + "," +  payloadMap.get(gpsEntity.getLatitude());
        }

        //封装返回结果
        if(location!=null){
            DeviceLocation deviceLocation=new DeviceLocation();
            deviceLocation.setDeviceId(deviceId);
            deviceLocation.setLocation(location);
            return deviceLocation;
        }else{
            return  null;
        }
    }

    @Override
    public GPS getGps() {
        return this.getById(1);
    }

    @Override
    public List<DeviceFullInfo> getDeviceFullInfo(Double lat, Double lon, Integer distance) {
        //按范围查询设备
        List<DeviceLocation> deviceLocationList = esRepository.searchDeviceLocation(lat, lon, distance);

        List<DeviceFullInfo> deviceFullInfoList= Lists.newArrayList();

        //查询设备详情
        deviceLocationList.forEach( deviceLocation -> {
            DeviceFullInfo deviceFullInfo=new DeviceFullInfo();
            deviceFullInfo.setDeviceId(deviceLocation.getDeviceId());//设备id
            deviceFullInfo.setLocation(deviceLocation.getLocation());//坐标

            //在线状态和告警状态

            DeviceDTO deviceDTO = esRepository.searchDeviceById(deviceLocation.getDeviceId());
            if(deviceDTO==null){
                deviceFullInfo.setOnline(false);
                deviceFullInfo.setAlarm(false);
            }else{
                deviceFullInfo.setOnline( deviceDTO.getOnline() );
                deviceFullInfo.setAlarm(deviceDTO.getAlarm());
            }

            //指标
            List<QuotaInfo> quotaList = quotaService.getLastQuotaList(deviceLocation.getDeviceId());
            deviceFullInfo.setQuotaList( quotaList );

            deviceFullInfoList.add(deviceFullInfo);
        } );

        return deviceFullInfoList;
    }

    @Override
    public boolean update(GPS gps) {
        try {
            emqClient.subscribe( "$queue/"+gps.getSubject() );
            System.out.println( "订阅gps主题：" + gps.getSubject()  );
        } catch (MqttException e) {
            e.printStackTrace();
        }

        gps.setId(1);
        return this.updateById(gps);
    }
}
