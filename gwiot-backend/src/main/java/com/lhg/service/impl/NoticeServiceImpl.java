package com.lhg.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.lhg.config.WebHookConfig;
import com.lhg.constant.Constants;
import com.lhg.framework.emq.EmqClient;
import com.lhg.pojo.dto.AlarmMsg;
import com.lhg.pojo.dto.DeviceLocation;
import com.lhg.pojo.dto.QuotaDTO;
import com.lhg.service.NoticeService;
import com.lhg.utils.HttpUtil;
import com.lhg.utils.JsonUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private EmqClient emqClient;

    @Override
    public void quotaTransfer(List<QuotaDTO> quotaDTOList) {
        for( QuotaDTO quotaDTO:quotaDTOList  ){
            //指标透传
            if(!Strings.isNullOrEmpty( quotaDTO.getWebhook() )){
                HttpUtil.httpPost( quotaDTO.getWebhook(),quotaDTO );
            }
            //告警透传
            if( "1".equals( quotaDTO.getAlarm()  ) && !Strings.isNullOrEmpty( quotaDTO.getAlarmWebHook() )  ){
                // key: XXXXX_设备id_告警名称
                String key= Constants.CYCLE_KEY+"_"+ quotaDTO.getDeviceId()+"_"+quotaDTO.getAlarmName();
                if( redisTemplate.boundValueOps(key ).get()==null  ){
                    HttpUtil.httpPost( quotaDTO.getAlarmWebHook(),quotaDTO );
                    redisTemplate.boundValueOps(key).set( quotaDTO.getStringValue(),  quotaDTO.getCycle() , TimeUnit.MINUTES   );
                }
            }
            sendAlarm( quotaDTO );
        }
    }
    @Autowired
    private WebHookConfig webHookConfig;
    @Override
    public void onlineTransfer(String deviceId, Boolean online) {
        if(!Strings.isNullOrEmpty( webHookConfig.getOnline())){
            Map<String,Object> map= Maps.newHashMap();
            map.put("deviceId",deviceId);
            map.put("online",online);
            HttpUtil.httpPost(webHookConfig.getOnline() , map);
        }
    }

    @Override
    public void gpsTransfer(DeviceLocation deviceLocation) {
        if(!Strings.isNullOrEmpty( webHookConfig.getGps())){
            HttpUtil.httpPost( webHookConfig.getGps(), deviceLocation);
        }
    }

    @Override
    public void sendDeviceStatusToEmq(AlarmMsg alarmMsg) {
        //发送到emq
        try {
            emqClient.publish("/device/alarm", JsonUtil.serialize(alarmMsg)   );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 告警前端推送
     * @param quotaDTO
     */
    private void sendAlarm(QuotaDTO quotaDTO){
        if( !"1".equals( quotaDTO.getAlarm()  )){
            return;
        }
        AlarmMsg alarmMsg=new AlarmMsg();
        BeanUtils.copyProperties( quotaDTO,alarmMsg );
        alarmMsg.setLevel( Integer.parseInt(quotaDTO.getLevel() ) );
        alarmMsg.setOnline(true);

        //发送到emq
        try {
            emqClient.publish("/device/alarm", JsonUtil.serialize(alarmMsg)   );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
