package com.lhg.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhg.mapper.BoardMapper;
import com.lhg.pojo.entity.Board;
import com.lhg.service.BoardService;
import org.springframework.stereotype.Service;

@Service
public class BoardServiceImpl extends ServiceImpl<BoardMapper, Board> implements BoardService {
    @Override
    public Boolean disable(Integer boardId) {
        Board boardEntity = this.getById(boardId);
        if(boardEntity == null) {
            return false;
        }
        if(boardEntity.getSystem()){
            boardEntity.setDisable(true);
            return this.updateById(boardEntity);
        }

        return this.removeById(boardId);
    }
}
