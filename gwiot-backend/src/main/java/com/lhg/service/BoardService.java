package com.lhg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhg.pojo.entity.Board;

public interface BoardService extends IService<Board> {
    /**
     * 删除看板
     * @param boardId
     * @return
     */
    Boolean disable(Integer boardId);
}
