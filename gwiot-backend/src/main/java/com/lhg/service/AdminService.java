package com.lhg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhg.pojo.entity.User;

public interface AdminService extends IService<User>{
    /**
     * 登录
     * @param loginName
     * @param password
     * @return
     */
    Integer login(String loginName,String password);

    /**
     *
     * @param username 用户名
     * @return
     */
    User selectUserByUserName(String username);
}
