package com.lhg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhg.pojo.dto.DeviceFullInfo;
import com.lhg.pojo.dto.DeviceLocation;
import com.lhg.pojo.entity.GPS;

import java.util.List;
import java.util.Map;

public interface GpsService extends IService<GPS> {
    /**
     * 解析报文获得GPS信息
     * @param payloadMap 报文内容
     * @return gps
     */
    DeviceLocation analysis(String topic, Map<String, Object> payloadMap);

    GPS getGps();

    /**
     * 根据经纬度获取一定范围内的设备信息
     * @param lat
     * @param lon
     * @param distance
     * @return
     */
    List<DeviceFullInfo> getDeviceFullInfo(Double lat, Double lon, Integer distance);

    boolean update(GPS gps);
}
