package com.lhg.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lhg.pojo.dto.DeviceInfoDTO;
import com.lhg.pojo.dto.QuotaDTO;
import com.lhg.pojo.dto.QuotaInfo;
import com.lhg.pojo.entity.Quota;

import java.util.List;
import java.util.Map;

/**
 * @author 刘华桂
 */
public interface QuotaService  extends IService<Quota> {

    List<QuotaInfo> getLastQuotaList(String deviceId);

    List<String> getAllSubject();
    /**
     * 解析报文
     * @param topic 主题名称
     * @param payloadMap 报文内容
     * @return 设备（含指标列表）
     */
    DeviceInfoDTO analysis(String topic, Map<String, Object> payloadMap);

    /***
     * 保存指标数据到influxDb
     * @param quotaDTOList
     */
    void saveQuotaToInflux(List<QuotaDTO> quotaDTOList);

    IPage<Quota> queryNumberQuota(Long page, Long pageSize);

    IPage<Quota> queryPage(Long page, Long pageSize, String quotaName);
}
