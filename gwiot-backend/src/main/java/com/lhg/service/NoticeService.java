package com.lhg.service;

import com.lhg.pojo.dto.AlarmMsg;
import com.lhg.pojo.dto.DeviceLocation;
import com.lhg.pojo.dto.QuotaDTO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 刘华桂
 */

public interface NoticeService {
    /**
     * 指标数据透传
     * @param quotaDTOList
     */
    void quotaTransfer(List<QuotaDTO> quotaDTOList);

    /**
     * 断连透传
     * @param deviceId
     * @param online
     */
    void onlineTransfer(String deviceId,Boolean online  );

    /**
     * gps透传
     * @param deviceLocation
     */
    void gpsTransfer(DeviceLocation deviceLocation);

    void sendDeviceStatusToEmq(AlarmMsg alarmMsg);
}
