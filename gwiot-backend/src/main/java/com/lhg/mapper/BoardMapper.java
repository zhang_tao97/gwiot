package com.lhg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lhg.pojo.entity.Board;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BoardMapper extends BaseMapper<Board>{
}
