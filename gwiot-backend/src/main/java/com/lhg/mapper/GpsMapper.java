package com.lhg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lhg.pojo.entity.GPS;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;

@Mapper
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface GpsMapper extends BaseMapper<GPS>{
}
