package com.lhg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lhg.pojo.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper extends BaseMapper<User>{
}
