package com.lhg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lhg.pojo.entity.Alarm;
import org.apache.ibatis.annotations.*;

/**
 * @author 刘华桂
 */
@Mapper
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface AlarmMapper extends BaseMapper<Alarm>{
    @Results(id="alarmMap",value = {
            @Result(property = "quota",column = "quota_id",one = @One(select = "com.lhg.mapper.QuotaMapper.selectById")),
            @Result(property = "quotaId",column = "quota_id")
    })
    @Select("select * from tb_alarm where id=#{id}")
    Page<Alarm> queryPage(Page<Alarm> page, Integer id);
}
