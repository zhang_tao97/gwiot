package com.lhg.constant;

/**
 * @author 刘华桂
 */
public class Constants {
    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    //redis中指标主题前缀
    public static final String QUOTA_KEY_PREFIX = "gwIot.quota.";
    //redis中gps信息key前缀
    public static final String GPS_KEY_PREFIX = "gwIot.gps.quota.";
    //redis中客户端状态key前缀
    public static final String CLIENT_INFO = "gwIot.client.";

    //redis中客户端状态key前缀
    public static final String CYCLE_KEY = "gwIot.cycle.";

    //redis中设备存储key前缀
    public static final  String DEVICE_KEY = "v.device";
    //ES中设备索引名称
    public static final String ES_INDEX_NAME = "Device";
    //redis中通过主题存储的设备Id字段前缀
    public static final String QUOTA_SUBJECT_DEVICE_KEY_PREFIX = "gwIot.quota.deviceId.";

    /**
     * redis中设备指标存储key的前缀
     */
    public static final String DEVICE_QUOTA_KEY_PREFIX = "gwIot.device.quotaList.";
}
