package com.lhg.controller;

import com.lhg.pojo.dto.DeviceFullInfo;
import com.lhg.pojo.entity.GPS;
import com.lhg.service.GpsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 刘华桂
 */
@RestController
@RequestMapping("/gps")
public class GpsController {

    @Autowired
    private GpsService gpsService;

    /**
     * 修改gps指标
     * @param gps
     * @return
     */
    @PutMapping
    public boolean update(@RequestBody GPS gps){
        return gpsService.update(gps);
    }

    /**
     * 获取gps指标
     * @return
     */
    @GetMapping
    public GPS get(){
        return gpsService.getGps();
    }


    /**
     * 根据经纬度获取设备信息 Lat/Lon 就是/
     * @param lat latitude 经度
     * @param lon longitude 纬度
     * @param distance 范围
     * @return
     */
    @GetMapping("/deviceList/{lat}/{lon}/{distance}")
    public List<DeviceFullInfo> getDeviceFullInfo(
            @PathVariable Double lat,
            @PathVariable Double lon,
            @PathVariable Integer distance){
        return gpsService.getDeviceFullInfo(lat,lon,distance);
    }
}
