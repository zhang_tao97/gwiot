package com.lhg.controller;

import com.lhg.constant.Constants;
import com.lhg.pojo.vo.AdminVO;
import com.lhg.utils.AjaxResult;
import com.lhg.web.service.SysLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 刘华桂
 */
@RestController
public class LoginController {


    @Autowired
    private SysLoginService loginService;

    /**
     * 用于用户登入
     * @param admin 用户登入对象 用户名 密码
     * @return
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody AdminVO admin){
        AjaxResult ajax = AjaxResult.success();
        String token = loginService.login(admin.getLoginName(), admin.getPassword());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }
}
