package com.lhg.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lhg.exception.base.BaseException;
import com.lhg.framework.emq.EmqClient;
import com.lhg.exception.ServiceException;
import com.lhg.pojo.entity.Alarm;
import com.lhg.pojo.entity.Quota;
import com.lhg.pojo.vo.Pager;
import com.lhg.pojo.vo.QuotaVO;
import com.lhg.service.AlarmService;
import com.lhg.service.QuotaService;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;

/**
 * @author 刘华桂
 */
@Slf4j
@RestController
@RequestMapping("/quota")
public class QuotaController {
    @Autowired
    private QuotaService quotaService;

    @Autowired
    private AlarmService alarmService;

    @Autowired
    private EmqClient emqClient;

    /**
     * 用于创建一个新的设备指标
     * @param vo 设备指标波
     * @return 是否保存成功 TRUE保存成功
     */
    @PostMapping
    public boolean create(@RequestBody QuotaVO vo){
        try {
            Quota quotaEntity = new Quota();
            BeanUtils.copyProperties(vo,quotaEntity);
            // 添加这句！
            emqClient.subscribe("$queue/"+quotaEntity.getSubject());

            return quotaService.save(quotaEntity);
        }catch (DuplicateKeyException e){
            throw new ServiceException("已存在该名称");
        } catch (MqttException e) {
            log.error("订阅主题失败",e);
            return false;
        }
    }

    /**
     * 分页获取所有指标
     * @param page
     * @param pageSize
     * @param quotaName
     * @return
     */
    @GetMapping
    public Pager<Quota> queryPage(@RequestParam(value = "page",required = false,defaultValue = "1") Long page,
                                        @RequestParam(value = "pageSize",required = false,defaultValue = "10") Long pageSize,
                                        @RequestParam(value = "quotaName",required = false) String quotaName){
        return new Pager<>(quotaService.queryPage(page,pageSize,quotaName));
    }

    /**
     * 更新指标
     * @param vo
     * @return
     */
    @PutMapping
    public Boolean update(@RequestBody QuotaVO vo){
        try {
            Quota entity = new Quota();
            BeanUtils.copyProperties(vo,entity);

            return quotaService.updateById(entity);
        }catch (DuplicateKeyException e){
            throw new BaseException("已存在该名称");
        }

    }


    /**
     * 分页获取数值型指标
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/numberQuota")
    public Pager<Quota> queryNumberQuota(@RequestParam(value = "page",required = false,defaultValue = "1") Long page,
                                               @RequestParam(value = "pageSize",required = false,defaultValue = "10") Long pageSize){
        return new Pager<>(quotaService.queryNumberQuota(page,pageSize));
    }

    /**
     * 删除指标
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable Integer id){
        QueryWrapper<Alarm> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Alarm::getQuotaId,id);
        Integer count = alarmService.count(queryWrapper);
        if(count>0) {
            throw new BaseException("该指标使用中");
        }
        return quotaService.removeById(id);
    }



}
