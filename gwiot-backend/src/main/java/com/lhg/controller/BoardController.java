package com.lhg.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.lhg.exception.base.BaseException;
import com.lhg.pojo.entity.Board;
import com.lhg.pojo.vo.BoardStatus;
import com.lhg.pojo.vo.BoardVO;
import com.lhg.service.BoardService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 刘华桂
 */
@RestController
@RequestMapping("/board")
public class BoardController {
    @Autowired
    private BoardService boardService;

    /**
     * 添加面板
     * @param vo 面板vo
     * @return
     */
    @PostMapping
    public boolean add(@RequestBody BoardVO vo){
        try {
            Board entity = new Board();
            BeanUtils.copyProperties(vo,entity);

            return boardService.save(entity);
        }catch (DuplicateKeyException e){
            throw new BaseException("已存在该名称");
        }

    }

    /**
     * 获取所有面板
     * @return
     */
    @GetMapping()
    public List<Board> getAll(){
        QueryWrapper<Board> wrapper = new QueryWrapper<>();
        wrapper
                .lambda()
                .eq(Board::getSystem,false);
        return boardService.list(wrapper);
    }

    /**
     * 获取系统看板
     * @return
     */
    @GetMapping("/systemBoard")
    public List<Board> getSystemBoard(){
        QueryWrapper<Board> wrapper = new QueryWrapper<>();
        wrapper
                .lambda()
                .eq(Board::getDisable,false)
                .eq(Board::getSystem,true);

        return boardService.list(wrapper);
    }

    /**
     * 删除看板
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable Integer id){
        Board board = boardService.getById(id);
        if(board.getSystem()){
            BoardStatus boardStatus = new BoardStatus();
            boardStatus.setBoardId(id);
            boardStatus.setDisable(true);
            List<BoardStatus> boardStatuses = Lists.newArrayList();
            boardStatuses.add(boardStatus);
            return setStatus(boardStatuses);
        }
        return boardService.removeById(id);
    }

    /**
     * 更新面板
     * @param board 面板对象
     * @return 是否更新成功
     */
    @PutMapping
    public Boolean update(@RequestBody Board board){
        try {
            return boardService.updateById(board);
        }catch (DuplicateKeyException e){
            throw new BaseException("已存在该名称");
        }

    }

    /**
     * 设置面板状态
     * @param boardStatusList 面板对象列表
     * @return
     */
    @PutMapping("/status")
    public Boolean setStatus(@RequestBody List<BoardStatus> boardStatusList){
        boardStatusList.forEach(x->{
            Board board = boardService.getById(x.getBoardId());
            board.setDisable(x.getDisable());
            boardService.updateById(board);
        });

        return true;
    }
}
