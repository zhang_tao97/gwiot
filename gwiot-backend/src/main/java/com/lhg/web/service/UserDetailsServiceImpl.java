package com.lhg.web.service;

import com.lhg.pojo.entity.User;
import com.lhg.pojo.model.LoginUser;
import com.lhg.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户验证处理
 *
 * @author liuhuagui
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
    @Autowired
    private AdminService adminService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 查询数据库是否有这个用户
        User user = adminService.selectUserByUserName(username);
        return createLoginUser(user);
    }

    public UserDetails createLoginUser(User user)
    {
        return new LoginUser(user);
    }
}
