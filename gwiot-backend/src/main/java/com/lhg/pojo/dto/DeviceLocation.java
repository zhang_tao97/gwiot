package com.lhg.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeviceLocation implements Serializable {
    private String deviceId;
    private String location;
}
