package com.lhg.pojo.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DeviceFullInfo implements Serializable {
    private List<QuotaInfo> quotaList;
    private String location;
    private String deviceId;
    private Boolean alarm;
    private Boolean online;
}
