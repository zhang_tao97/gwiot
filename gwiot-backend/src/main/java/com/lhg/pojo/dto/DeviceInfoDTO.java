package com.lhg.pojo.dto;

import lombok.Data;

import java.util.List;


/**
 * 用于封装一个设备对应多个指标信息
 * @author 刘华桂
 */
@Data
public class DeviceInfoDTO {
    /**
     * 设备
     */
    private DeviceDTO device;

    /**
     * 指标列表
     */
    private List<QuotaDTO> quotaList;

}
