package com.lhg.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * @author 刘华桂
 */
@Data
public class LineVO {
    /**x轴*/
    private List<String> xdata;
    /**数据*/
    private List<Long> series;
}
