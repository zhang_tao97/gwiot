package com.lhg.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 用于保存设备指标
 * @author 刘华桂
 */
@Data
public class QuotaVO implements Serializable{
    /**id*/
    private Integer id;

    /**指标名称*/
    private String name;

    /**指标单位*/
    private String unit;

    /**订阅主题*/
    private String subject;

    private String valueKey;

    private String snKey;

    private String webhook;

    private String referenceValue;

    private String valueType;
}
