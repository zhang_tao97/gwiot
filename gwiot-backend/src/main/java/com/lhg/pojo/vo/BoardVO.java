package com.lhg.pojo.vo;

import lombok.Data;

/**
 * @author 刘华桂
 */
@Data
public class BoardVO {
    private Integer id;

    /**管理员id*/
    private Integer adminId;

    /**面板名称*/
    private String name;

    /**关注的指标*/
    private Integer quota;

    /**设备编号*/
    private String device;

    /**是否为系统面板*/
    private Boolean system;
}
