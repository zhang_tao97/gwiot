package com.lhg.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 面板指标数据
 * @author 刘华桂
 */
@Data
public class BoardQuotaData implements Serializable {
    /**
     * 名称（设备编号）
     */
    private String name;
    /**
     * 指标数据
     */
    private List<Double> data;
}
