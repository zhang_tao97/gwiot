package com.lhg.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 面板VO对象
 * @author 刘华桂
 */
@Data
public class BoardQuotaVO implements Serializable {

    /**
     * x轴数据
     */
    private List<String> xdata;

    /**
     * Y轴数据
     */
    private List<BoardQuotaData> series;

    /**
     * 面板名称
     */
    private String name;
}
