package com.lhg.pojo.vo;

import lombok.Data;

/**
 * @author 刘华桂
 */
@Data
public class PieVO {
    private String name;//名称

    private Long value;//值
}
