package com.lhg.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * @author 刘华桂
 */
@Data
public class PreviewVO {
    private String quotaId;
    private List<String> deviceIdList;
    private Integer type;
    private String start;
    private String end;
}
