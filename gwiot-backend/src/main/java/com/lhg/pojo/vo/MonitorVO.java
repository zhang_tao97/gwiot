package com.lhg.pojo.vo;

import lombok.Data;

/**
 * @author 刘华桂
 */
@Data
public class MonitorVO {
    /**
     * 设备数量
     */
    private Long deviceCount;
    /**
     * 告警设备数
     */
    private Long alarmCount;
}
