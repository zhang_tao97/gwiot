package com.lhg.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 刘华桂
 */
@TableName(value = "t_alarm",resultMap = "alarmMap",autoResultMap = true)
@Data
public class Alarm implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String name;
    private Integer quotaId;
    private String operator;
    private Integer threshold;
    private Integer level;
    private Integer cycle;
    @TableField(value = "webhook")
    private String webHook;
    @TableField(exist = false)
    private Quota quota;
}
