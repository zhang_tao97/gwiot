package com.lhg.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@TableName(value = "t_user")
@Data
public class User implements Serializable{
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String loginName;
    private String password;
    private Boolean type;
    private String board;
}
