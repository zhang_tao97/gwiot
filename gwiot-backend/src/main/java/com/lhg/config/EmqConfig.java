package com.lhg.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 刘华桂
 */
@Configuration
@ConfigurationProperties("emq")
@Data
public class EmqConfig {
    /** MQTT服务URL*/
    private String mqttServerUrl;
}
