package com.lhg;

import com.lhg.framework.es.ESRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class testCount {

    @Autowired
    private ESRepository esRepository;

    @Test
    public void testCount(){

        Long allDeviceCount = esRepository.getAllDeviceCount();//设备总数
        System.out.println("设备总数："+allDeviceCount);

        Long offlineCount = esRepository.getOfflineCount();//离线设备数量
        System.out.println("离线设备："+offlineCount);

        Long alarmCount = esRepository.getAlarmCount();//告警设备数量
        System.out.println("告警设备："+alarmCount);

    }
}
