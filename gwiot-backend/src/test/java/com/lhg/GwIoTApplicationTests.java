package com.lhg;

import com.lhg.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class GwIoTApplicationTests {

    @Autowired
    public TokenService tokenService;

    @Test
    void contextLoads() {

    }

}
