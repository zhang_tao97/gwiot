package com.lhg;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lhg.pojo.dto.TrendPoint;
import com.lhg.service.ReportService;
import com.lhg.utils.JsonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest(classes = GwIoTApplication.class)
@RunWith(SpringRunner.class)
public class TestReport {

    @Autowired
    private ReportService reportService;

    @Test
    public void testAlarmTrend(){

        List<TrendPoint> trendPointList = reportService.getAlarmTrend("2022-03-25", "2022-04-01", 3);

        for(TrendPoint trendPoint:trendPointList){
            try {
                System.out.println(JsonUtil.serialize(trendPoint));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

    }
}
